#!/usr/bin/env bash

export HELM_EXPERIMENTAL_OCI=1

GITLAB_REGISTRY="registry.gitlab.com"
GITLAB_USER="edelmatthias"

HELM_CHART_SOURCE_REGISTRY=registry.gitlab.com/edelmatthias/test-me-helm-charts

CHART_VERSION="0.4.0"

helm registry login -p ${GITLAB_USER} -u "" ${GITLAB_REGISTRY}

helm chart pull ${HELM_CHART_SOURCE_REGISTRY}:test-me-${CHART_VERSION}
helm chart export ${HELM_CHART_SOURCE_REGISTRY}:test-me-${CHART_VERSION} -d charts/
#helm chart pull ${HELM_CHART_SOURCE_REGISTRY}:test-me-k8s-deployment-${TFV_CHART_VERSION}
#helm chart export ${HELM_CHART_SOURCE_REGISTRY}:test-me-k8s-deployment-${TFV_CHART_VERSION} -d charts/

